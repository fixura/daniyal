export function getAccountName(value) {
    switch (value) {
        case 'FUNDING_ACCOUNT': {
            return 'Funding Account'
        }
        default: {
            return value
        }
    }
}

export function getAccountType(value) {
    switch (value) {
        case 'INVESTOR_ACCOUNT': {
            return 'Investor Account'
        }
        default: {
            return value
        }
    }
}