import React, { useState, useEffect } from 'react';
import * as Actions from './store/actions';
import withReducer from '../store/withReducer';
import reducer from './store/reducers';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Icon, Typography } from '@material-ui/core';
import ReactSelect from "react-select";
import * as Utils from '../../../Utils';

/* 
Feature render / fetch user investment and sets in investmentDetail state.
API invoked in imported actions.
*/
function UserInvestmentList() {

    const dispatch = useDispatch();

    const userId = 145127236;
    
    const [investmentDetail, setInvestmentDetail] = useState('');

    const data = useSelector(UserInvestmentListApp => UserInvestmentListApp.userInvestment.list)

    useEffect(() => {
        dispatch(
            Actions.getUserInvestmentList(userId)
        );
    }, [userId, dispatch]);


    const userInvestmentListMaps = () => {
        if (data) {
            const ArrayOptions = data.map(userInvestment => {
                return { value: userInvestment, label: userInvestment.name + ' - ' + userInvestment.id };
            });
            return ArrayOptions
        } else {
            return []
        }
    };

    const handleUserInvestment = (event) => {
        setInvestmentDetail(event.value)
    }

    return (
        <>
            <div className="card">
                <ExpansionPanel
                    key="user-investment-list"
                    defaultExpanded={true}
                >
                    <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
                        <div className="w-full flex items-center">
                            <Icon className="mr-8 custom-blue">monetization_on</Icon>
                            <Typography className="custom-blue">User Investments</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <>
                            <div className="w-full p-8">
                                <div className="list">
                                    <ReactSelect
                                        className="select-user-investment"
                                        placeholder="Select User Investment ..."
                                        id={"userInvestment"}
                                        name="userInvestment"
                                        onChange={handleUserInvestment}
                                        options={userInvestmentListMaps()}
                                        components={{
                                            LoadingIndicator: null,
                                            DropdownIndicator: () => null,
                                            IndicatorSeparator: () => null
                                        }}
                                        noOptionsMessage={() => null}
                                        closeMenuOnSelect={true}
                                    />
                                    <div className="item">
                                        <Typography>
                                            Total
                                        </Typography>
                                        <div className="text-10 ml-4">
                                            {data ? data.length : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
            <div className="card">
                <ExpansionPanel
                    key="user-investment-detail"
                    defaultExpanded={true}
                >
                    <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
                        <div className="w-full flex items-center">
                            <Icon className="mr-8 custom-blue">credit_card</Icon>
                            <Typography className="custom-blue">Detail: {investmentDetail.id}</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    {investmentDetail ? (
                        <ExpansionPanelDetails>
                            <div className="w-full p-8">
                                <div className="list">
                                    <div className="item">
                                        <Typography>
                                            ID
                                         </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.id}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Account Holder Type
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.accountHolderType}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Name
                                         </Typography>
                                        <Typography className="text-10 ml-4">
                                            {Utils.getAccountName(investmentDetail.name)}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Account Type
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {Utils.getAccountType(investmentDetail.accountType)}
                                        </Typography>
                                    </div>
                                </div>
                                <div className="list">
                                    <div className="item">
                                        <Typography>
                                            Balance
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.balance}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Allow Over draft
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.allowOverdraft ? '✅' : '❌'}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Locked Balance
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.lockedBalance}
                                        </Typography>
                                    </div>
                                    <div className="item">
                                        <Typography>
                                            Available Balance
                                        </Typography>
                                        <Typography className="text-10 ml-4">
                                            {investmentDetail.availableBalance}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                        </ExpansionPanelDetails>
                    ) :
                        <div className="w-full p-8">
                            <div className="list">
                                <div className="item">
                                    <div className="font-bold p-8">
                                        Select User Investment
                        </div>
                                </div>
                            </div>
                        </div>}
                </ExpansionPanel>
            </div>
        </>
    )
}

export default withReducer('UserInvestmentListApp', reducer)(UserInvestmentList);