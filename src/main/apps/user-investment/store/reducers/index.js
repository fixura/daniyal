import {combineReducers} from 'redux';
import userInvestment from './userInvestmentList.reducer';
import userInfo from './userInfo.reducer';

const createReducer = (asyncReducers) =>
    combineReducers({
        userInvestment,
        userInfo,
        ...asyncReducers
    });

export default createReducer;