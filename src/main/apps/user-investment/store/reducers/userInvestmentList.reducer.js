import * as Actions from '../actions';

const initialState = {
    list: undefined,
};

const userInvestmentListReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_USER_INVESTMENT_LIST:
            {
                return {
                    ...state,
                    list: action.payload,
                };
            }
        default:
            {
                return state;
            }
    }
};

export default userInvestmentListReducer;
