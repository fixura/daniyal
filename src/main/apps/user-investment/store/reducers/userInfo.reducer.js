import * as Actions from '../actions';

const initialState = {
    user: undefined,
};

const userInvestmentListReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_USER_INFO:
            {
                return {
                    ...state,
                    user: action.payload,
                };
            }
        default:
            {
                return state;
            }
    }
};

export default userInvestmentListReducer;
