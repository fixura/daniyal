import axios from 'axios';

export const GET_USER_INFO = '[USER INVESTMENT APP] GET USER INFO';

export function getUserInfo(userId) {
    const request = axios.get(`https://qmkjbvj35b.execute-api.eu-north-1.amazonaws.com/get/user/${userId}/`, {
        headers: {
            'x-api-key': 'ifGIBNTP5b4TVSbDylpST5vhabFJCSBJ7fqmMSKB'
        }
    });

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_USER_INFO,
                payload: response.data
            })
        });
}
