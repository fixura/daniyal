import axios from 'axios';

export const GET_USER_INVESTMENT_LIST = '[USER INVESTMENT APP] GET USER INVESTMENT LIST';

export function getUserInvestmentList(userId) {
    const request = axios.get(`https://qmkjbvj35b.execute-api.eu-north-1.amazonaws.com/get/user/${userId}/investments`, {
        headers: {
            'x-api-key': 'ifGIBNTP5b4TVSbDylpST5vhabFJCSBJ7fqmMSKB'
        }
    });

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_USER_INVESTMENT_LIST,
                payload: response.data
            })
        });
}
