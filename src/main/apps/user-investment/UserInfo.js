import React, { useEffect } from 'react';
import * as Actions from './store/actions';
import withReducer from '../store/withReducer';
import reducer from './store/reducers';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Icon, Typography } from '@material-ui/core';
import moment from 'moment';

/* 
Feature render / fetch user profile and sets in user state.
API invoked in actions.
*/
function UserInfo(props) {

    const dispatch = useDispatch();

    const userId = 145127236;

    const user = useSelector(UserInfoApp => UserInfoApp.userInfo.user)

    useEffect(() => {
        dispatch(
            Actions.getUserInfo(userId)
        );
    }, [userId, dispatch]);

    return (
        <div className="card mt-10">
            {user && (
                <ExpansionPanel
                    key="user-info"
                    defaultExpanded={true}
                >
                    <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
                        <div className="w-full flex items-center">
                            <Icon className="mr-8 custom-blue">person_outline</Icon>
                            <Typography className="custom-blue">User Profile</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails >
                        <div className="w-full p-8">
                            <div className="list">
                                <div className="item">
                                    <Typography>
                                        First Name
                                     </Typography>
                                    <Typography className="text-10 ml-4">
                                        {user.firstName}
                                    </Typography>
                                </div>
                                <div className="item">
                                    <Typography>
                                        Last Name
                                    </Typography>
                                    <Typography className="text-10 ml-4">
                                        {user.lastName}
                                    </Typography>
                                </div>
                                <div className="item">
                                    <Typography>
                                        Gender
                                     </Typography>
                                    <Typography className="text-10 ml-4">
                                        {user.gender}
                                    </Typography>
                                </div>
                                <div className="item">
                                    <Typography>
                                        Email
                                     </Typography>
                                    <Typography className="text-10 ml-4">
                                        {user.emailAddress}
                                    </Typography>
                                </div>
                            </div>
                            <div className="list">
                                <div className="item">
                                    <Typography>
                                        Date of Birth
                                    </Typography>
                                    <Typography className="text-10 ml-4">
                                        {moment(user.birthDate).format('DD.MM.YYYY')}
                                    </Typography>
                                </div>
                                <div className="item">
                                    <Typography>
                                        Language
                                    </Typography>
                                    <Typography className="text-10 ml-4">
                                        {user.preferredLanguage}
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            )}
        </div>
    )
}

export default withReducer('UserInfoApp', reducer)(UserInfo);