import React from 'react';
import UserInvestmentList from './user-investment/UserInvestmentList.js';
import UserInfo from './user-investment/UserInfo.js';

function UserInvestor(props) {
    return (
        <>
            <UserInfo />
            <UserInvestmentList />
        </>
    )
}

export default UserInvestor;