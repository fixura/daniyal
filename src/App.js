import React from 'react';
import './App.css';
import store from './main/apps/store';
import AppContext from './AppContext';
import UserInvestor from './main/apps/UserInvestor.js';
import Provider from 'react-redux/es/components/Provider';

function App() {
  return (
    <AppContext.Provider>
      <Provider store={store}>
        <UserInvestor />
      </Provider>
    </AppContext.Provider>
  );
}

export default App;
